import { IProposito } from './../../shared/components/proposito/IProposito';
import { Component, OnInit } from '@angular/core';

import { NOSSA_GENTE_DATA } from './../../shared/components/nossa-gente/nossa-gente.constants';
import { ICarouselItem } from 'src/app/shared/components/nossa-gente/ICarousel-item.metadata';
import { PROPOSITO_DATA } from 'src/app/shared/components/proposito/proposito.constants';
import { IDiversidade } from './diversidade/IDiversidade';
import { DIVERSIDADE_DATA } from './diversidade/diversidade.constants';

@Component({
  selector: 'app-corporativo',
  templateUrl: './corporativo.component.html',
  styleUrls: ['./corporativo.component.scss']
})
export class CorporativoComponent implements OnInit {

  isDisplay : boolean = true;

  public carouselData: ICarouselItem[] = NOSSA_GENTE_DATA;
  public propositoData: IProposito[] = PROPOSITO_DATA;
  public diversidadeData: IDiversidade[] = DIVERSIDADE_DATA;

  oportunidades: Array<any> = [
    {
      id: 1,
      title: 'ANALISTA COMERCIAL',
      local: 'SÃO PAULO - SP',
      setor: 'CORPORATIVO',
      tipo: 'PRESENCIAL ',
      contrato: 'CLT - EFETIVO',
      genero: 'TODES',
      color: 'blue',
      spikeColor: 'spike-blue',
      icon: '/assets/oportunidades_briefcase.svg',
    },
    {
      id: 2,
      title: 'ANALISTA COMERCIAL PL',
      local: 'SÃO PAULO - SP',
      setor: 'CORPORATIVO',
      tipo: 'PRESENCIAL ',
      contrato: 'CLT - EFETIVO',
      genero: 'TODES',
      color: 'blue',
      spikeColor: 'spike-blue',
      icon: '/assets/oportunidades_briefcase.svg',
    },
    {
      id: 3,
      title: 'ANALISTA COMERCIAL',
      local: 'SÃO PAULO - SP',
      setor: 'CORPORATIVO',
      tipo: 'PRESENCIAL ',
      contrato: 'CLT - EFETIVO',
      genero: 'TODES',
      color: 'blue',
      spikeColor: 'spike-blue',
      icon: '/assets/oportunidades_briefcase.svg',
    },
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
