export interface IDiversidade {
  id: number;
  title?: string;
  width?: number;
  image: string;
  link?: string;
  order?: number;
  marginLeft?: number;
}