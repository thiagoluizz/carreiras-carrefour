import { IProposito } from './../../shared/components/proposito/IProposito';
import { Component, OnInit } from '@angular/core';
import { NOSSA_GENTE_DATA } from './../../shared/components/nossa-gente/nossa-gente.constants';
import { ICarouselItem } from 'src/app/shared/components/nossa-gente/ICarousel-item.metadata';
import { PROPOSITO_DATA } from 'src/app/shared/components/proposito/proposito.constants';
// import { faBars } from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  // btnMobile = faBars;
  isDisplay: boolean = true;

  public carouselData: ICarouselItem[] = NOSSA_GENTE_DATA;
  public propositoData: IProposito[] = PROPOSITO_DATA;

  oportunidades: Array<any> = [
    {
      id: 1,
      title: 'ANALISTA COMERCIAL',
      local: 'SÃO PAULO - SP',
      setor: 'CORPORATIVO',
      tipo: 'PRESENCIAL ',
      contrato: 'CLT - EFETIVO',
      genero: 'TODES',
      color: 'blue',
      spikeColor: 'spike-blue',
      icon: '/assets/oportunidades_briefcase.svg',
    },
    {
      id: 2,
      title: 'ANALISTA COMERCIAL PLENO',
      local: 'São José do Vale do Rio Preto - RJ',
      setor: 'VAREJO',
      tipo: 'REMOTO ',
      contrato: 'CLT - EFETIVO',
      genero: 'TODES',
      color: 'blue',
      spikeColor: 'spike-blue',
      icon: '/assets/oportunidades_briefcase.svg',
    },
    {
      id: 3,
      title: 'ANALISTA COMERCIAL SENIOR',
      local: 'são joãos da boa vista - SP',
      setor: 'PROPERTY',
      tipo: 'REMOTO ',
      contrato: 'CLT - EFETIVO',
      genero: 'TODES',
      color: 'green',
      spikeColor: 'spike-green',
      icon: '/assets/oportunidades_splash.svg',
    },
    {
      id: 4,
      title: 'ANALISTA COMERCIAL ESPECIALISTA EM VAREJO',
      local: 'SÃO PAULO - SP',
      setor: 'CORPORATIVO',
      tipo: 'PRESENCIAL ',
      contrato: 'CLT - EFETIVO',
      genero: 'TODES',
      color: 'lightblue',
      spikeColor: 'spike-lightblue',
      icon: '/assets/oportunidades_pigbank.svg',
    },
    {
      id: 5,
      title: 'ANALISTA COMERCIAL',
      local: 'SÃO PAULO - SP',
      setor: 'CORPORATIVO',
      tipo: 'PRESENCIAL ',
      contrato: 'CLT - EFETIVO',
      genero: 'TODES',
      color: 'blue',
      spikeColor: 'spike-blue',
      icon: '/assets/oportunidades_buycart.svg',
    },
    {
      id: 6,
      title: 'ANALISTA COMERCIAL',
      local: 'SÃO PAULO - SP',
      setor: 'CORPORATIVO',
      tipo: 'PRESENCIAL ',
      contrato: 'CLT - EFETIVO',
      genero: 'TODES',
      color: 'orange',
      spikeColor: 'spike-orange',
      icon: '/assets/oportunidades_storage.svg',
    },
  ];

  constructor() {}

  ngOnInit(): void {}

  // toggleDisplay() {
  //   this.isDisplay = !this.isDisplay
  // }
}
