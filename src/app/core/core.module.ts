import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {MatDialogModule} from '@angular/material/dialog';
// import { SwiperModule } from 'swiper/angular';

// import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
  declarations: [],
  imports: [CommonModule, BrowserModule, MatDialogModule],
  exports: [BrowserModule, MatDialogModule ],
})
export class CoreModule {}
