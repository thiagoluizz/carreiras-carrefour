import { CoreModule } from './core/core.module';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { PropositoComponent } from './shared/components/proposito/proposito.component';
import { HeaderComponent } from './shared/components/header/header.component';
import { FooterComponent } from './shared/components/footer/footer.component';
import { NavbarComponent } from './shared/components/navbar/navbar.component';
import { NossaGenteComponent } from './shared/components/nossa-gente/nossa-gente.component';
import { CardComponent } from './shared/components/card/card.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SetoresComponent } from './pages/home/setores/setores.component';
import { CorporativoComponent } from './pages/corporativo/corporativo.component';
import { SetorComponent } from './pages/corporativo/setor/setor.component';
import { DiversidadeComponent } from './pages/corporativo/diversidade/diversidade.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    PropositoComponent,
    FooterComponent,
    NavbarComponent,
    NossaGenteComponent,
    CardComponent,
    SetoresComponent,
    CorporativoComponent,
    SetorComponent,
    DiversidadeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CoreModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
