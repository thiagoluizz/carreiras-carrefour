import { Component, OnInit } from '@angular/core';
import { faBars } from "@fortawesome/free-solid-svg-icons";


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  btnMobile = faBars;
  isDisplay : boolean = true;

  constructor() { }

  ngOnInit(): void {
  }

  toggleDisplay() {
    this.isDisplay = !this.isDisplay
  }

}

