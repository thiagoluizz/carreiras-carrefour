import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})
export class CardComponent implements OnInit {
  oportunidades: Array<any> = [
    {
      id: 1,
      title: 'ANALISTA COMERCIAL',
      local: 'SÃO PAULO - SP',
      setor: 'CORPORATIVO',
      tipo: 'PRESENCIAL ',
      contrato: 'CLT - EFETIVO',
      genero: 'TODES',
    },
    {
      id: 2,
      title: 'ANALISTA COMERCIAL PLENO',
      local: 'SÃO JOSÉ DOS CAMPOS - SP',
      setor: 'VAREJO',
      tipo: 'REMOTO ',
      contrato: 'CLT - EFETIVO',
      genero: 'TODES',
    },
    {
      id: 3,
      title: 'ANALISTA COMERCIAL SENIOR',
      local: 'CAMPINAS - SP',
      setor: 'PROPERTY',
      tipo: 'REMOTO ',
      contrato: 'CLT - EFETIVO',
      genero: 'TODES',
    },
    {
      id: 4,
      title: 'ANALISTA COMERCIAL ESPECIALISTA EM VAREJO',
      local: 'SÃO PAULO - SP',
      setor: 'CORPORATIVO',
      tipo: 'PRESENCIAL ',
      contrato: 'CLT - EFETIVO',
      genero: 'TODES',
    },
    {
      id: 5,
      title: 'ANALISTA COMERCIAL',
      local: 'SÃO PAULO - SP',
      setor: 'CORPORATIVO',
      tipo: 'PRESENCIAL ',
      contrato: 'CLT - EFETIVO',
      genero: 'TODES',
    },
    {
      id: 6,
      title: 'ANALISTA COMERCIAL',
      local: 'SÃO PAULO - SP',
      setor: 'CORPORATIVO',
      tipo: 'PRESENCIAL ',
      contrato: 'CLT - EFETIVO',
      genero: 'TODES',
    },
  ];

  constructor() {}

  ngOnInit(): void {}
}
