export interface ICarouselItem {
  id: number;
  title?: string;
  cargo?: string;
  width?: number;
  image: string;
  link: string;
  order?: number;
  marginLeft?: number;
}