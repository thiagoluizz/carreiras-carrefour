import { ICarouselItem } from './ICarousel-item.metadata';

export const NOSSA_GENTE_DATA: ICarouselItem[] = [
  {
    id: 1,
    title: 'João da Silva',
    cargo: 'Analista de Varejo',
    link: '/',
    image: '../../../../assets/78_79_Hiper_Villa_Lobos_SPV_018.jpg',
  },
  {
    id: 2,
    title: 'JULIANA PEREIRA DOS SANTOS',
    cargo: 'ANALISTA FINANCEIRO PL',
    link: '/',
    image: '../../../../assets/055_Banco_Carrefour.jpg',
  },
  {
    id: 3,
    title: 'MARIA OLIVEIRA',
    cargo: 'TECH LEAD',
    link: '/',
    image: '../../../../assets/055_CeBB_waltercraveiro.jpg',
  },
];
