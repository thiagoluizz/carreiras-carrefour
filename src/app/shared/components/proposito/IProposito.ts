export interface IProposito {
  id: number;
  title?: string;
  width?: number;
  image: string;
  link?: string;
  order?: number;
  marginLeft?: number;
}