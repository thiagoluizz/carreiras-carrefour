import { IProposito } from './IProposito';

export const PROPOSITO_DATA: IProposito[] = [
  {
    id: 1,
    title: 'João da Silva',
    link: '/',
    image: '../../../../assets/ScreenShot_2021-06-03_Frame_Video-Propo╠БsitoCarrefour.png',
  },
  {
    id: 2,
    title: 'JULIANA PEREIRA DOS SANTOS',
    link: '/',
    image: '../../../../assets/055_Banco_Carrefour.jpg',
  },
  {
    id: 3,
    title: 'MARIA OLIVEIRA',
    link: '/',
    image: '../../../../assets/055_CeBB_waltercraveiro.jpg',
  },
];
